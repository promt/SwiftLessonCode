//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

/*
 1、类的定义
 2、创建一个类的对象
 3、给对对象的属性赋值
 */

class Student: NSObject {
    var age = 0
    var name:String?
    
    var Chinese = 81.5
    var Math:Double = 30
    var Eng = 59.9
    func avg() -> Double {
        let num = (self.Chinese+Math+Eng)/3.0
        return num
    }
    var Avg:Double{
        get{
        return 0+(Chinese+Math+Eng)/3.0
        }
//        set{
//        Avg = 100
//        }
    
    //return (Chinese+Math+Eng)/3.0
    }
    
    static var major:String = "iOS"
    
    override var description: String{
        if let _name = name{
        return "\(self.age),\(_name),\(self.Eng)"
        }else{
        return "GG"
        }
    }
}
var stu:Student = Student()

stu.age = 22
stu.name = "xiaoming"

print(stu.Chinese)

stu.avg()
stu.Math = 60.5
stu.Avg

//stu.Avg = 120
print(stu.Avg)

Student.major
Student.major = "Android"
Student.major

print(stu)



