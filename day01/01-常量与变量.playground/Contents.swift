//: Playground - noun: a place where people can play


/*
 Swift特色
 1、在Swift中可以不适用分号作为语句结尾，但是必须报保证一行之后一个语句，多个语句处于同一行必须已分号隔开
 2、在Swift中定义一个数据时，可以不预先规定数据的类型，编译器会根据后面设置的值自动推导出数据类型
 3、定义一个数据时，如果没有预先给定一个值，就必须预先规定数据类型，并用冒号隔开----(如果初始值没有提供足够的信息(或者没有初始值),那你需要在变量后面声明类型,用冒号分割。)
 //
 */










import UIKit

var str = "Hello, playground"

/*
 1、在Swift中，定义常量或者变量时，使用let/var
 2、使用let定义常量，常量一旦设置之后值不可改变
 3、使用var定义变量
 */


let chang = 1
//chang = 2



var chang2    = 2.0
chang2 = 3.0



//let num1 : Int  num2 : Int

let num3 : Int
let num4 : Double
let num5:Double = 5.0


//"\()"相当于OC中的格式化占位符,需要打印的东西写在括号内
print("num5=\(num5)")



