//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

/*
 if
 1、可以省略判断语句的小括号（）
 2、无论如何都不得省略执行语句的大括号
 3、在其他类如OC，C，Java中的“非0即真”不再适用
 4、if中的判断表达式结果必须是bool类型的数据(true/false)
 */
let num = 10

if (num==10){
    print(num)
}
if num==10{
    print("num=\(num)")
}

if num==10
{print("sss")}

if num != 10{
    print("sss")
}

if true{
    print(true)
}
if false{
    print("flae")
}

let i = 0

if i == 1{
    
}else{
    print("sss");
}
if i == 1{
    
}else if i==2{
    print("sss");
}else{
    print("i!=1 i!=2")
}

/*
 1、判断的标记量小括号可以省略
 2、不在像OC，C的Swich 没有break会穿透执行下一个case
 3、default必须写在最后
 4、在 Swift中default在“大多数”情况下都不得省略
 5、在case中定义一个新的量时不需要再像OC中的那样必须添加大括号来确定量的作用域
 */
let ret = 2
switch ret{
case 1:
    print("ret=\(ret)")
case 2:
    print("ret=\(ret)")
    let it = 10
    print(it)
    break
case 3:
    print("ret=\(ret)")
    break
case 4:
    print("ret=\(ret)")
    break
case 5:
    print("ret=\(ret)")
    break
default:print("default")
}




