//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
//“@”取消
//{} -> []

//创建了一个不可变字典
let dict1 = ["key":"value"]

let dict2 = Dictionary<NSObject,AnyObject>()

let dict3 = [NSObject:AnyObject]()


//创建可变字典

var dict4 = Dictionary<NSObject,AnyObject>()

var dictM = [NSObject:AnyObject]()

//字典的基本操作
//add
dictM["FC"] = "jkyb"
dictM["name"] = "jkyb"
dictM["type"] = "jkyb"
dictM["id"] = "jkyb"
dictM["class"] = "jkyb"
dictM["bingo"] = "0000"
dictM

//del

dictM.removeValueForKey("bingo")
dictM

dictM.removeAll()
dictM

//update
dictM["bingo"] = "1001"

//取值

dictM["bingo"]

//遍历
dictM["FC"] = "jkyb"
dictM["name"] = "jkyb"
dictM["type"] = "jkyb"
dictM["id"] = "jkyb"
dictM["class"] = "jkyb"
dictM["bingo"] = "0000"

//遍历key
for key in dictM.keys{
    print(key)
}
//遍历value
for value in dictM.values{
    print(value)
}
//遍历key-value
for (key,value) in dictM{
    print(key)
    print(value)
}

//合并 注意：无论是什么类型的数据 都不直接 + ，必须遍历添加

var  dictA = ["A":1,"B":2]
let  dictB = ["a":1,"b":2]

//let dictAB = dictA + dictB
for (key,value) in dictB{
dictA[key] = value
}
dictA





