//
//  ViewController.swift
//  TableView
//
//  Created by LiynXu on 2016/9/26.
//  Copyright © 2016年 LiynXu. All rights reserved.
//

import UIKit

class ViewController: UIViewController{

    /*
     1、创建一个tableView
     2、设置Frame，添加视图
     3、设置delegate
     4、设置dataSource
     5、实现协议方法
     */
    
    //创建tableView属性
    var tableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        setupTableView()
        
    }

    


}

extension ViewController{
    // MARK:- 注释#UI相关
    func setupTableView() {
        //设置frame
        tableView.frame = view.bounds
        tableView.backgroundColor = UIColor.redColor()
        view.addSubview(tableView)
        //设置代理
        
        tableView.delegate = self
        //设置数据源
        
        tableView.dataSource = self
    }
    
    
    
}

//MARK:- #
extension ViewController:UITableViewDelegate,UITableViewDataSource {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        //cell最好是Var修饰，因为其地址可能会发生变化
        var cell = tableView.dequeueReusableCellWithIdentifier("CellID")
        
        /*
         枚举类型的取值
         1、枚举类型.具体类型
         2、.具体类型
         
         */
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: "CellID")
        }
        
        cell?.textLabel?.text = "\(indexPath.row)"
        
        //一般情况下，我们可以直接写上!，因为在此处Cell肯定有值
        return cell!
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("\(indexPath.row)")
    }
}

