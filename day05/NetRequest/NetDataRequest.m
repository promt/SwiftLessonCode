//
//  NetDataRequest.m
//  NetDataRequest
//
//  Created by LiynXu on 2016/9/24.
//  Copyright © 2016年 LiynXu. All rights reserved.
//

#import "NetDataRequest.h"
#import <AFNetworking.h>
@interface NetDataRequest()
@property (nonatomic,strong) AFHTTPSessionManager *manager;
@end
@implementation NetDataRequest
+(instancetype)standRequest{
    static NetDataRequest *net = nil;
    if (net == nil) {
        net = [[NetDataRequest alloc] init];
    }
    return net;
}
- (instancetype)init{
    if (self = [super init]) {
        _manager = [AFHTTPSessionManager manager];
        _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }
    return self;
}
-(void)GET:( NSString *)string parameters:( id)parameters progress:(void(^)(NSProgress *progress))progress  success:(void(^)(NSObject *requestData))success  failure: (void(^)(NSError *error))failure{
    [_manager GET:string parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        progress(downloadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
    
}

+ (void)GET:(NSString *)string parameters:(id)parameters progress:(void (^)(NSProgress *))Progress success:(void (^)(NSObject *))Success failure:(void (^)(NSError *))Failure{
    NetDataRequest *request = [NetDataRequest standRequest];
    [request GET:string parameters:parameters progress:^(NSProgress *progress) {
        Progress(progress);
    } success:^(NSObject *requestData) {
        Success(requestData);
    } failure:^(NSError *error) {
        Failure(error);
    }];
}


@end
