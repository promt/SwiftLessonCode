//
//  ViewController.swift
//  ClassFromString
//
//  Created by LiynXu on 2016/9/26.
//  Copyright © 2016年 LiynXu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let tempNameSpace = NSBundle.mainBundle().infoDictionary!["CFBundleExecutable"]
        
        let nameSpace =  tempNameSpace as? String
        
        print(nameSpace)
        
        
        //print(NSBundle.mainBundle().infoDictionary!)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //        let vc = NewViewController()
        //        vc.view.backgroundColor = UIColor.redColor()
        //        self.navigationController?.pushViewController(vc, animated: true)
        
        guard let tempNameSpace = NSBundle.mainBundle().infoDictionary!["CFBundleExecutable"] as? String else {
            print("命名空间未找到")
            return
        }
        CustomLog(tempNameSpace)
        guard let newVc = NSClassFromString("\(tempNameSpace)"+"."+"NewViewController") else{
            CustomLog("无法获取当前字符串所对应的类")
            return
        }
        CustomLog(newVc)
        
        guard let NewVCType = newVc as? UIViewController.Type else{
        
            CustomLog("未找到当前字符串对应的类的具体类型")
            return
        }
        CustomLog(NewVCType)
        
        
        let newViewC = NewVCType.init()
        
        CustomLog(UIViewController.Type)
        
        self.navigationController?.pushViewController(newViewC, animated: true)
        
        //let vc = NewViewController()
        
        
    }
    
}

