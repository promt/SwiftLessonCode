//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
/*
 1、简单的if-else结构语句可以转换为三目运算
 2、三目运算看起来比if else更加的简洁
 3、三目运算在Swift开发中是用的频率非常的高
 */
let num=5
if num==5{
    print("num=5")
}else{
    print("num!=5")
}

let  num2 = (num != 5) ? num : 15

var num3 = 0

if num != 5{
    num3 = num
}else{
    num3 = 15
}
print(num3)

/*
 1、可选类型的值可以有也可以没有
 2、在Swift开发中？!经常遇见，？代表这个数据是个可选类型数据，！代表这个数据一定有值，又叫强制解析，如果对一个没有值的可选类型进行强制解析，那么程序只有一个结果----崩溃
 3、可选类型数据使用之前要判断是否有值，避免强制解析触发崩溃
 4、可以使用可选绑定的形式对可选类型数据的值进行转移，有值的情况才会执行{}内的语句
 */
//let flag:Int?

let url = NSURL(string:"https://ww.baidu.com")
print(url)
print(url!)

let  url2 = NSURL(string:"https://ww.baidu.com/嘿嘿嘿")
print(url2)

//print(url2!)
//let urlRequset = NSURLRequest(URL: url2!)

if url2 != nil{
    print(url2!)
    let urlRequset = NSURLRequest(URL: url2!)
}
//可选绑定

if let Url = url2{
    let urlRequset = NSURLRequest(URL: Url)
}



        
