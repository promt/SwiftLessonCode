//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

class Person: NSObject {
    var age = 0
    var name:String?
    //重写父类的init()方法
    //init()方法会自动被系统调用
    
    override init() {
        super.init()
        //super.init()可以写也可以不写，如果不写系统会自动添加至当前类的init()方法最后一行
        //所以建议手动写在当前类的init()方法的第一行
        print("init")
    }
    
    init(Age:Int,Name:String) {
        self.age = Age;
        self.name = Name
        //内部参数名称和属性名不能一样
        age = Age
        name = Name
    }
    
    
    init(dict:[String:AnyObject]) {
        //self.name = dict["name"] as! String
        //as? 最终转换的数据类型是可选类型，值可以为空
        //as! 最终转换成的数据类型是确定类型，确定有值
        if let name = dict["name"] as? String{
            self.name = name
        }
    }
    override var description: String{
        if let name = self.name{
            return "Age=\(self.age),Name=\(name)"
        }else {
            return "GG"
        }
    
    }
    
    func setValues(Age Age:Int,Name:String){
        age = Age
        name = Name
    }
}

var person1 = Person()
var person2 = Person(Age: 22, Name: "Worker")
print(person2.name)

var person3 = Person(dict: ["name":"XiaoMing"])
print(person3.name)

if let name = person3.name{
print(name)
}

print(person3)


var person4 = Person()
person4.setValues(Age:88, Name: "Older")

print(person4)

