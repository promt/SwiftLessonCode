//
//  ViewController.swift
//  5-5-懒加载
//
//  Created by LiynXu on 2016/9/24.
//  Copyright © 2016年 LiynXu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var dataArray:Array<NSObject> = {
        //var array = Array<NSObject>()
        
        print("LazyArray")
        return Array<NSObject>()
    }();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sleep(3)
        
        print(self.dataArray)
        
    }
    

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.dataArray.append("A")
        print(self.dataArray)
    }
}

