//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//类型转换
let num1 = 5
let num2 = 10.0
let num3 = num1+Int(num2)

let num4:Double = Double(num1)+num2+Double(num3)

let num5:Int = num1+Int(num2)+num3


/*
 (值永远不会被隐式转换为其他类型。如果你需要把一个值转换成其他类型,must显式转换。)
 1、不同类型的数据不可以进行运算
 2、如需运算必须保证表达式中所有 数据为同一类型数据
 3、类型转换必须是显性 ---（因为编译器不给你转类型）
 */


let num0:Int? = 0
print(num0)

var num10:Int?
print(num10=10)

