//
//  Dog.swift
//  5-3-闭包的循环引用
//
//  Created by LiynXu on 2016/9/24.
//  Copyright © 2016年 LiynXu. All rights reserved.
//

import UIKit

class Dog: NSObject {

    var block:((gutou:NSObject)->())?
    
    func funning(block block:(gutou:NSObject)->()) -> String {
        
        self.block = block;
        let obj = NSObject()
        
        //sleep(1)
        block(gutou: obj)
        
        return "好开森"
    }
    
}
