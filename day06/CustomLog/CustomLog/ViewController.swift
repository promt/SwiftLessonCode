//
//  ViewController.swift
//  CustomLog
//
//  Created by LiynXu on 2016/9/26.
//  Copyright © 2016年 LiynXu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("Log");
        
        let funcName = #function
        print(funcName)
        
        //line在哪获取的打印的是哪一行
        //let lineNum = #line
        
        print(#line)
        
        let fileName = (#file as NSString).lastPathComponent
        
        print(fileName)
        
        
        
        let  log  = "\(fileName):\(funcName):\(#line)"
        print(log)
        
        
       
        CustomLog(999)
        
    }
    

    


}

