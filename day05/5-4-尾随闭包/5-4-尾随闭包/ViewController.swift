//
//  ViewController.swift
//  5-4-尾随闭包
//
//  Created by LiynXu on 2016/9/24.
//  Copyright © 2016年 LiynXu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        //尾随闭包：在函数的参数列表中，最后一个参数是闭包的情况下，我们可以把最后一个闭包写成尾随闭包的形式
        //形式1、正常写法 所有参数都在()内
        self.autumn({(num)->Void in
        
        } )
        //形式2、除了最后一个闭包以外，所有的参数都在()内
        self.autumn() { (num) -> (Void) in
            
        }
        //形式3、单参数为闭包时，直接在函数名后写闭包 
        self.autumn { (num) -> (Void) in
            
        }
    }

    func autumn(block:(num:Int)->(Void)) {
        
        print(timeval)
    }
   
}

