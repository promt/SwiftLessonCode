//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

/*
 1、可选类型的值可以有也可以没有
 2、在Swift开发中？!经常遇见，？代表这个数据是个可选类型数据，！代表这个数据一定有值，又叫强制解析，如果对一个没有值的可选类型进行强制解析，那么程序只有一个结果----崩溃
 3、可选类型数据使用之前要判断是否有值，避免强制解析触发崩溃
 4、可以使用可选绑定的形式对可选类型数据的值进行转移，有值的情况才会执行{}内的语句
 */
//最基本的定义方式
var value:Optional<NSObject> = nil
value = "sss"

//语法糖----常用的方式
var value2:NSObject?
value2 = "SOS"


var value3:NSObject? = nil
value3 = "SOS"

print(value)
print(value2!)
print(value3)

//使用之前必须判断是否有值

if let name = value2{
    print(name)
}









