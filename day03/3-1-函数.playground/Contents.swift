//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//无参无返回

func printABC() {
    print("abcdefg hijklmn opq rst uvw xyz")
}
print("ABC")
printABC()

//无参有返回
//一般的getter方法都是

func sum() -> Int {
    return 2+3
}
sum()


//有参无返回
//大多数setter方法都是

func sum(num1 num1:Int,num2:Int) -> Void {
    print("\(num1)+\(num2) = \(num2+num1)")
}
sum(num1: 2, num2: 1)

//有参有返回
//异常常见

func sum(start start:Int,step:Int,times:Int) -> Int {
    var sum = 0
    var num = start
    
    for _ in 0..<times {
        sum += num
        num = num+step
        
    }
    
    return sum
}

sum(start: 1, step: 1, times: 100)















