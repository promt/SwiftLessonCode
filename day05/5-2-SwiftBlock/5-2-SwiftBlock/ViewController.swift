//
//  ViewController.swift
//  5-2-SwiftBlock
//
//  Created by LiynXu on 2016/9/24.
//  Copyright © 2016年 LiynXu. All rights reserved.
//

import UIKit



class ViewController: UIViewController {

    var name:String?
    
    var SwiftBlock:((URL:String)->(String))?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(SwiftBlock)
        //创建一个swfitBlock
        
        self.SwiftBlock = { (URL) -> (String) in
            return URL
        }
        //参数传入
        print(self.Block(aBlock: SwiftBlock!))
        
        print(SwiftBlock!)
    }
    
    func aBlock(Block Block:(URL:String)->(String))-> String{
        
        sleep(2)
        return Block(URL: "啥地方规划局快乐；")
    }
    func Block(aBlock aBlock:(URL:String)->(String))-> String{
        //SwiftBlock = aBlock;
        sleep(2)
        let string = aBlock(URL: "hhh")
        
        return string;
        //return aBlock(URL: "啥地方规划局快乐；")
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       //闭包 (参数列表) -> (Void)
       //block___ Void(^)(参数列表)
        let flag = self.Block(aBlock: SwiftBlock!)
        print(flag)
        
    }

    


}

