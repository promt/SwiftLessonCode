//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

func sum(num1 num1:Int,num2:Int,num3:Int) -> Int {
    return num1+num2+num3
}
sum(num1:2, num2: 3,num3: 3)

//注意一：外部参数与内部参数
/*
 在函数内部可以看得到的参数，就是内部参数
 在函数外部可以看得到的参数，就是外部参数
 默认情况下，从第二个参数开始，参数名称既是内部参数也是外部参数
 如果第一个参数也要有外部参数，可以设置标签：在变量名前加标签即可
 如果不想要外部参数，可以在参数名称前加_
 */
//func sum(_ num1:Int,_ num2:Int,_ num3:Int) -> Int {
//    return num1+num2+num3
//}
//sum(2,3,4)

//注意二：默认参数

func buyDinner(name:String = "米饭") -> String {
    return name
    //return 10
}


buyDinner("炒饭")
buyDinner("帝王蟹")
buyDinner("盖饭")
buyDinner("")
buyDinner()

//注意三：可变参数

func sum(num1 num1:Int,num2:Int) -> Int {
    return num1+num2
}
sum(num1:2, num2: 3)
//sum(2, 3, 3)

func sum(num:Int...) -> Int {
    var sum = 0
    for i in num {
        sum+=i
    }
    return sum
}

sum(1,2,3,4,5,6,7,8,9,10)

//注意四：指针类型

var x = 5
var y = 10
func swap( var num1:Int,var num2:Int) {
    print("\(num1),\(num2)")
    let temp = num1
    num1 = num2
    num2 = temp
    print("\(num1),\(num2)")
}
print("x=\(x),y=\(y)")

swap(x, num2: y)
func swap(  inout num1 num1:Int,inout num2:Int) {
    print("\(num1),\(num2)")
    let temp = num1
    num1 = num2
    num2 = temp
    print("\(num1),\(num2)")
}

swap(num1:&x, num2: &y)
print("x=\(x),y=\(y)")

//注意五：函数的嵌套

func test(name:String) -> String {
    
   
    print(name)
    testB("nameB")
    return name
}
func testB(name:String) -> String{
    print(name+"b")
    return name+"B"
}
test("nameA")








