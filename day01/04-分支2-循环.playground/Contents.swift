//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

/*
 1、for关键字后的小括号可以省略
 2、使用区间来确定循环的次数
 3、如果可以不关注标记变量的值，我们可以使用“_”来替代标记变量,"_"使用频率非常高
 */
//以前的for循环
for (var i=0; i<10; i++){
    print(i)
}
//swift中的for循环
for var i=0;i<20;i++
{
print("i=\(i)")
}
//++ --> +=
for var i=0;i<20;i+=1
{
    print("i=\(i)")
}

//区间：取值范围
//"..<" 前闭后开

for var i in 0..<10{
print("i=\(i)")
}

//“...” 闭区间
for var i in 0 ... 10{
print("aaa")
}
//不关注标记变量的值 (swift独有)
for _ in 0...10{
print("bbb")
}





var flag = 0

while (flag<10)
{
    print("flag")
    flag+=1
}

while flag<20
{
    print("flag")
    flag+=1
}
//do-while中的do被作为了关键字占用，所以使用repeat代替do
repeat{
    print(flag)
    flag+=1
}while flag<30






