//
//  NetDataRequest.h
//  NetDataRequest
//
//  Created by LiynXu on 2016/9/24.
//  Copyright © 2016年 LiynXu. All rights reserved.
//



#import <Foundation/Foundation.h>

/*
 [_manager GET:<#(nonnull NSString *)#> parameters:<#(nullable id)#> progress:<#^(NSProgress * _Nonnull downloadProgress)downloadProgress#> success: failure:<#^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)failure#>]
 }

 */
@interface NetDataRequest : NSObject
+(instancetype)standRequest;
-(void)GET:( NSString *)string parameters:( id)parameters progress:(void(^)(NSProgress *progress))progress  success:(void(^)(NSObject *requestData))success  failure: (void(^)(NSError *error))failure;
+(void)GET:( NSString *)string parameters:( id)parameters progress:(void(^)(NSProgress *progress))Progress  success:(void(^)(NSObject *requestData))Success  failure: (void(^)(NSError *error))Failure;
@end

