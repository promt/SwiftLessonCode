//
//  ViewController.swift
//  5-6-BasicView
//
//  Created by LiynXu on 2016/9/24.
//  Copyright © 2016年 LiynXu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let button = UIButton()
        button.frame = CGRectMake(100, 100, 100, 35)
        button.backgroundColor = UIColor.redColor()
        button.layer.cornerRadius = 5
        button.setTitle("button", forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(ViewController.hhh), forControlEvents: UIControlEvents.TouchUpInside)
        
        //button.addTarget(self, action:Selector("hhh") , forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(button)
        
        let label = UILabel()
        label.frame = CGRectMake(0, 0, 200, 30)
        label.center = self.view.center
        label.text = "abdcefg"
        label.textColor = UIColor.whiteColor()
        label.adjustsFontSizeToFitWidth = true
        label.font = UIFont.systemFontOfSize(25)
        label.textAlignment = NSTextAlignment.Center
        label.backgroundColor = UIColor.blackColor()
        let tap = UITapGestureRecognizer(target: self, action: #selector(ViewController.tap))
        label.userInteractionEnabled = true
        label.addGestureRecognizer(tap)
        self.view.addSubview(label)
        
        

        
    }
    
    func hhh() -> Void {
        print("hhh")
    }
    func tap() {
        print("tap")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

