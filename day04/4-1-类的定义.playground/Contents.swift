//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//定义类的基本形式
//class ClassName: 父类 {
//    //定义属性以及方法
//}

/*
 1、类的定义
 2、创建一个类的对象
 3、给对对象的属性赋值
    1、直接赋值
    2、KVC（必须继承自NSObject）
 */

class Person: NSObject {
    
    var age:Int = 0
    var name:String? = nil
    
    //override  作为重写父类方法的标记
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        
    }
    
    
    
}


var per = Person()
var aView = UIView()

per.age = 25
print(per.age)

per.name = "John"

print(per.name!)

//KVC
per.setValuesForKeysWithDictionary(["age":22,"name":"Peter","height":1.88])
print(per.age)
print(per.name)










