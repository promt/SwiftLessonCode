//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

class Student:NSObject{
    var age:Int = 0
    var name:String?{
        willSet{
            print(newValue!)
            print("willSet")
        }
        didSet{
            print(oldValue)
            print("didSet")
        }
    }
    
}

var stu = Student()
//let _name:String? = nil
stu.name = "student"

stu.name = "Agent"



