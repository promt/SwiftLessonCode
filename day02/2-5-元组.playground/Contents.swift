//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//使用合适的形式来存储以下数据
/*
 name: "SX"
 age: 22
 weight: 100
 height: 1.55
 */

//以字典的形式存储
var dict = ["name":"SX","age":22,"weight":100,"height":1.55]
//以数组的形式存储
var array = ["SX",22,100,1.55]

//如何定义一个元组存储以上数据

var person = ("SX",22,100,1.55)

person.0
//给元素起别名
var otherPerson = (name:"SX",age:22,weight:100,height:1.55)
otherPerson.name
otherPerson.0
//使用别名定义元组
var (name,age,weight,height) = ("SX",22,100,1.55)
//(name:"SX",age:22,weight:100,height:1.55)
name
age
weight
height

name = "SC"
person.0 = "EC"















