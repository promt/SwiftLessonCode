//
//  ViewController.swift
//  5-3-闭包的循环引用
//
//  Created by LiynXu on 2016/9/24.
//  Copyright © 2016年 LiynXu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var aBlock:((title:String)->(String))?
    var bBlock:((title:String)->(String)) = {(title:String) -> String  in
        
        return title
    }
    var dog = Dog();
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //方法一、
//        weak var weakSelf = self;
//        
//       print( dog.funning { (gutou) in
//            weakSelf?.view.backgroundColor = UIColor.redColor()
//            print(gutou)
//        })
        //方法二、
        
        
//        print( dog.funning { [weak self] (gutou) in
//            self?.view.backgroundColor = UIColor.redColor()
//            print(gutou)
//            })
        
        //方法三、(不推荐)
        print( dog.funning { [unowned self] (gutou) in
            self.view.backgroundColor = UIColor.redColor()
            print(gutou)
            })
        //self.bBlock
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
//        self.aBlock = {(title:String) -> String  in
//            return title
//        }
//        self.dun(n:2,block:self.aBlock!)
    }
//    func dun(n num:Int, block:(title:String)->(String)) ->  String{
//        
//        return block(title: self.title!)
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit{
    print("dealloc")
    }

}

