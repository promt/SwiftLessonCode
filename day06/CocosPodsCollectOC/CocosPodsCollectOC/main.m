//
//  main.m
//  CocosPodsCollectOC
//
//  Created by LiynXu on 2016/9/26.
//  Copyright © 2016年 LiynXu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
